/* global Swiper moment OverlayScrollbars Rellax*/
'use strict';
$(document).ready(function() {

	function hiddenScroll() {
		if ($(document).height() > $(window).height()) {
			let scrollTop = $('html').scrollTop() ? $('html').scrollTop() : $('body').scrollTop();
			$('html').addClass('no-scroll').css('top', -scrollTop);
		}
	}

	function visibleScroll() {
		let scrollTop = parseInt($('html').css('top'));
		$('html').removeClass('no-scroll');
		$('html, body').scrollTop(-scrollTop);
	}

	// Init popups
	(function initPopups() {
		const btnOpenPopups = $('.js-popup-button');
		const btnClosePopups = $('.js-close-popup');
		const popupGame = $('#popup-game');
		const gameMine = $('#game-mine');

		function initPopup() {
			btnOpenPopups.on('click', function(e) {
				e.preventDefault();
				if (!gameMine.hasClass('js-popup-show')) $('.popup').removeClass('js-popup-show');
				let popupClass = `.${$(this).attr('data-popup')}`;
				$(popupClass).addClass('js-popup-show');
				if (!$('html').hasClass('no-scroll')) hiddenScroll();
			});
			closePopup();
		}

		function closePopup() {
			btnClosePopups.on('click', function(e) {
				e.preventDefault();
				$('.popup').removeClass('js-popup-show');
				if ($('html').hasClass('no-scroll') && popupGame.hasClass('game-ready')) return;
				if ($('html').hasClass('no-scroll')) visibleScroll();
			});
		}
		initPopup();
	}) ();

	// Init dropdown
	(function initDropdown() {
		const dropdown = $('.dropdown');

		dropdown.on('click', '.js-btn-dropdown', function(e) {
			e.preventDefault();
			let list = $(this)
				.closest('.dropdown')
				.find('.dropdown__inner')
				.toggleClass('js-dropdown-open');

			if (list[0].style.maxHeight) list[0].style.maxHeight = null;
			else list[0].style.maxHeight = `${list[0].scrollHeight}px`;

			if (list.hasClass('js-dropdown-open')) {
				let width = $(window).width();

				$(window).on('resize.dropdown', function() {
					let widthResize = $(window).width();

					if (width !== widthResize) {
						list.removeClass('js-dropdown-open');
						list[0].style.maxHeight = null;
					} else {
						return $(window).off('resize.dropdown');
					}
				});
			}
		});
	}) ();

	// Init more shops
	(function initMoreShops() {
		const wrap = $('#shop-list');
		const parentWrap = wrap.parent();
		const link = wrap.find('.shop-link');
		const dropdown = wrap.find('.dropdown');
		const dropdownBtnImg = dropdown.find('.shop-list__btn-dropdown img');
		const dropdownList = dropdown.find('.dropdown__list');
		link.clone()
			.removeClass('shop-item')
			.appendTo(dropdownList)
			.wrap('<li class="shop-item"></li>');
		const dropdownItems = dropdownList.find('li');
		const allItems = wrap.find('.shop-item');

		const doAdapt = () => {
			allItems.each(function() {
				$(this).removeClass('hidden');
			});

			let btnWidth = dropdown.outerWidth(true);
			let hiddenItems = [];
			const wrapWidth = parseInt(wrap.width());

			link.each(function(i, item) {
				if (wrapWidth >= btnWidth + $(item).outerWidth(true) + 5) {
					btnWidth += $(item).outerWidth(true) + 5;
				} else {
					$(item).addClass('hidden');
					hiddenItems.push(i);
				}
			});

			let addedSrc = false;

			if(!hiddenItems.length && !dropdown.hasClass('hidden')) {
				dropdown.addClass('hidden');
			} else {
				dropdownItems.each(function(i, item) {
					if (!~hiddenItems.indexOf(i)) $(item).addClass('hidden');

					if (!$(item).hasClass('hidden') && !addedSrc) {
						addedSrc = true;
						let curSrc = $(item).find('img').attr('src');
						dropdownBtnImg.attr('src', curSrc);
					}
				});
			}
		};

		setTimeout(() => {
			doAdapt();
			parentWrap.removeClass('header__top-inner--hidden');
		}, 50);

		$(window).on('resize.shops', function() {
			doAdapt();
		});

	}) ();

	// Open burger-menu
	(function openBurger() {
		const wrap = $('#header-bottom');
		const burger = wrap.find('.burger-menu');

		burger.on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('active')
				.siblings('.nav')
				.toggleClass('active');
		});
	}) ();

	// Init timer
	(() => {
		// Finish Date YYYY-MM-DD
		const countDownDate = moment('2019-08-17');

		const countdown_d = $('#countdown-d');
		const countdown_h = $('#countdown-h');
		const countdown_m = $('#countdown-m');
		const countdown_s = $('#countdown-s');

		// Method for normal view date
		const viewCount = count => count < 10 ? `0${count}` : count;

		const countDown = setInterval(() => {
			if (!$('.countdown').hasClass('active')) $('.countdown').addClass('active');

			const now = moment(new Date());
			const distance = countDownDate.diff(now);

			// Time calculations for days, hours, minutes and seconds
			const days = Math.floor(distance / (1000 * 60 * 60 * 24));
			const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			const seconds = Math.floor((distance % (1000 * 60)) / 1000);

			// Display the result in the elements
			countdown_d.text(days);
			countdown_h.text(viewCount(hours));
			countdown_m.text(viewCount(minutes));
			countdown_s.text(viewCount(seconds));

			// If the count down is finished, write some text
			if (distance < 0) {
				clearInterval(countDown);

				countdown_d.text('--');
				countdown_h.text('--');
				countdown_m.text('--');
				countdown_s.text('--');
			}
		}, 1000);
	})();

	// Anim for Head section
	(() => { if ($('.head__gold-img').length) $('.head__gold-img').addClass('active'); })();

	// Anim for Head chocolate
	(() => { if ($('.head__chocolate--animate').length) $('.head__chocolate--animate').addClass('active'); })();

	// Prize section slider
	(() => {
		new Swiper('.prize-main__content', {
			slidesPerView: 3,
			spaceBetween: 0,
			simulateTouch: false,
			navigation: {
				nextEl: '.prize-main__inner .swiper-custom-button-next',
				prevEl: '.prize-main__inner .swiper-custom-button-prev',
			},
			breakpoints: {
				767: {
					slidesPerView: 2
				},
				479: {
					slidesPerView: 1
				}
			}
		});
	})();

	// Promo section slider
	(() => {
		new Swiper('.promo-info__content', {
			slidesPerView: 1,
			resistanceRatio: 0,
			navigation: {
				nextEl: '.promo-info__inner .swiper-custom-button-next',
				prevEl: '.promo-info__inner .swiper-custom-button-prev',
			},
		});
	})();

	// Init parallax
	(() => {
		$(window).on('scroll', function() {
			let scrolled = $(window).scrollTop();
			$('.coin-img--first, .coin-img--fourth').css('transform', `translateY(${(scrolled * .05)}px)`);
			$('.coin-img--second').css('transform', `translateY(${(scrolled * .1)}px)`);
			$('.coin-img--third').css('transform', `translateY(${(scrolled * .2)}px)`);
		});
	})();

	(() => {
		const wrap = $('.products-slider');

		new Swiper(wrap, {
			slidesPerView: 4,
			spaceBetween: 60,
			watchOverflow: true,
			centerInsufficientSlides: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				991: {
					slidesPerView: 3
				},
				767: {
					slidesPerView: 2
				},
				479: {
					slidesPerView: 1
				}
			}
		});
	}) ();

	// Init scroll
	const initScroll = (function() {
		const wrapTable = document.getElementById('winner-table');
		const wrapPopupTabel = document.getElementById('popup-winner-table');
		const faqInner = document.getElementById('faq-inner');
		
		const initCustomScrollWinners = () => initCustomScroll(wrapTable);
		const initCustomScrollPopupWinners = () => initCustomScroll(wrapPopupTabel);
		const initCustomScrollFaq = () => initCustomScroll(faqInner);

		function initCustomScroll(el) {
			OverlayScrollbars(el, {
				scrollbars: {
					touchSupport: false,
				}
			});
		}

		return {
			initCustomScrollWinners,
			initCustomScrollPopupWinners,
			initCustomScrollFaq
		};
	}) ();

	if ((Boolean(document.getElementById('winner-table')) !== Boolean(null))) initScroll.initCustomScrollWinners();
	if ((Boolean(document.getElementById('popup-winner-table')) !== Boolean(null))) initScroll.initCustomScrollPopupWinners();
	if ((Boolean(document.getElementById('faq-inner')) !== Boolean(null))) initScroll.initCustomScrollFaq();

	// init accordion
	(function initAccordion() {
		const list = $('#faq-inner').find('.faq__list');

		list.on('click', '.faq__title', function() {
			let $this = $(this);
			let parent = $this.parent();
			let content = $this.next();

			if (content.is(':visible')) {
				content.slideUp('fast');
				parent.removeClass('active');
			} else {
				content.slideDown('fast');
				parent.addClass('active');
			}
			
		});
	}) ();

	//Init mask
	(function initMask() {
		const inputCard = $('#search-card').find('input');
		const inputTel = $('input[type="tel"]');

		inputTel.mask('+7 (999) 999-9999', {
			autoclear: false
		});		

		inputCard.mask('9999 9999 9999 9999', {
			autoclear: false,
			placeholder: 'X'
		});
	}) ();

	// Init video
	const initVideo = (function() {
		const wrap = document.getElementById('winners-slider');

		// function playVideo() {
		// 	const videos = wrap.querySelectorAll('.winners__video');

		// 	$(videos).fancybox({
		// 		keyboard: false,
		// 		arrows: false,
		// 		infobar: false,
		// 		buttons: ['close'],
		// 		touch: false,
		// 		wheel: false,
		// 		youtube : {
		// 			showinfo: 0
		// 		},
		// 	});

		// 	for (let i = 0; i < videos.length; i++) {
		// 		let src = `https://img.youtube.com/vi/${videos[i].dataset.embed}/sddefault.jpg`;
		// 		let img = new Image();
		// 		img.addEventListener('load', function() {
		// 			videos[i].appendChild(img);
		// 		}, false);
		// 		img.src = src;
		// 	}
		// }

		function fnSlider() {
			new Swiper(wrap, {
				slidesPerView: 3,
				spaceBetween: 40,
				allowTouchMove: false,
				navigation: {
					nextEl: '.swiper-custom-button-next',
					prevEl: '.swiper-custom-button-prev',
				},
				breakpoints: {
					767: {
						slidesPerView: 1,
						spaceBetween: 15,
						allowTouchMove: true
					}
				}
			});
		}

		return {
			// playVideo: playVideo,
			fnSlider: fnSlider
		};
	}) ();

	if (document.getElementById('winners-slider') !== null) {
		// initVideo.playVideo();
		initVideo.fnSlider();
	}

	// Init calendar
	const initCalendar = (function() {
		const wrap = $('#calendar-wrap');

		function calendarFn() {
			const calendar = wrap.find('.calendar__input');

			let datepicker = calendar.datepicker({
				showOtherMonths: false,
				dateFormat: 'dd.mm.yyyy',
				position: '',
				offest: 0,
				autoClose: true
			}).data('datepicker');

			let curDate = datepicker.date;
			let day = curDate.getDate();
			let month = curDate.getMonth() + 1;
			let year = curDate.getFullYear();
			if (day < 10) day = `0${day}`;
			if (month < 10) month = `0${month}`;
			let startDate = `${day}.${month}.${year}`;
			calendar.val(startDate);
			let wrapDatepicker = $('#datepickers-container');
			wrapDatepicker.appendTo(wrap);
		}

		return {
			calendarFn: calendarFn
		};

	}) ();

	if (document.getElementById('calendar-wrap') !== null) initCalendar.calendarFn();

	(() => {
		$('.head__countdown-close').on('click', () => {
			$('.head__countdown').remove();
		});
	})();
	
	// init notifications
	(function initNotif() {
		const notif = $('.notification__item');

		notif.find('.notification__close').on('click', function(e) {
			e.preventDefault();
			if ($(this).parent().hasClass('active')) $(this).parent().removeClass('active');
		});
	}) ();

	// add active btn
	(function addActiveBtn() {
		const wrap = $('#search');
		let inputTel = wrap.find('input[type="tel"]');
		let inputCard = wrap.find('input[type="text"]');
		let btn = wrap.find('button');
		let regPhone = /\+7\s\(\d{3}\)\s\d{3}-\d{4}/;
		let regCard = /\d{4}\s\d{4}\s\d{4}\s\d{4}/;
		
		inputTel.on('keyup', inputFn.bind(inputTel, btn, regPhone));
		inputCard.on('keyup', inputFn.bind(inputCard, btn, regCard));

		function inputFn(el, reg) {
			let $this = $(this);
			let value = $this.val();
			let valid = reg.test(value);
			
			if (valid && !el.hasClass('active')) el.addClass('active');
			else if (!valid && el.hasClass('active')) el.removeClass('active'); 
		}
	}) ();

	// init select2
	(function initSelect2() {
		const select = $('select');

		select.each(function() {
			let $this = $(this);
			let parent = $this.parent();
			let selectTheme = parent.find('select[name="theme"]');
			let selectShop = parent.find('select[name="shops"]');
			
			customSelectFn(selectTheme, 'Выберете тему');
			customSelectFn(selectShop, 'Выбрать');

			function customSelectFn(select, str) {
				select.select2({
					placeholder: str,
					minimumResultsForSearch: -1,
					dropdownParent: parent
				});
			}
		});


	}) ();

	// open checks
	(function openChecks() {
		const checks = $('.box-checks__img');

		checks.fancybox({
			keyboard: false,
			arrows: false,
			infobar: false,
			buttons: ['close'],
			touch: false,
			wheel: false
		});
	}) ();

	// init tabs
	(function initTabs() {
		const wrap = $('#profile-center');
		const tabs = wrap.find('.tabs');
		const tabsItem = wrap.find('.tabs__item--tab');
		const tabsContent = wrap.find('.tabs-content__item');

		tabs.on('click', '.tabs__item--tab', function(e) {
			e.preventDefault();
			let $this = $(this);
			let i = $this.index('.tabs__item--tab');
			
			tabsItem.removeClass('active');
			$this.addClass('active');
			tabsContent.removeClass('active').eq(i).addClass('active');
		});
	}) ();

	// Cookies
	const cookieFn = (function() {
		let cookies = $('.cookies');
		let date = new Date();
		date.setMonth(date.getMonth() + 1);
		
		cookies.find('.btn--cookies').on('click', function(e) {
			e.preventDefault();
			cookies.removeClass('js-popup-show');
			document.cookie = `AlpenGold=Game; path=/; expires=${date.toUTCString()}`;			
		});

		function getCookie(name) {
			let matches = document.cookie.match(new RegExp(
				`(?:^|; )${name.replace(/([.$?*|{}()[]\\\/\+^])/g, '\\$1')}=([^;]*)`
			));
			return matches ? decodeURIComponent(matches[1]) : undefined;
		}

		return {
			getCookie: getCookie
		};
	}) ();

	// click animate
	const clickAnimate = (function() {
		const wrap = $('#profile');
		const btnScroll = wrap.find('.tabs__item--play a');
		const btnStartGame = wrap.find('.btn--play');
		const popupGame = $('#popup-game');
		const btnCloseGame = popupGame.find('.popup__close--game');
		const point = $('#popup-points').find('.popup__point-num[data-point]');
		let cookies = $('.cookies');
		let count = 0;
	
		function scrollTo() {
			btnScroll.on('click', function() {
				let wrapHeight = wrap.outerHeight();
				let wrapPos = wrap.offset().top;
				let viewportHeight = $(window).height();
				
				$('html, body').stop().animate({
					scrollTop: wrapHeight + wrapPos - viewportHeight
				}, 700, function() {
					initAnimate();
				});
				return false;
			});
		}

		function playGame() {			
			btnStartGame.on('click', function(e) {
				e.preventDefault();
				initAnimate();
			});
		}

		function openGame() {
			popupGame.addClass('game-ready');
			hiddenScroll();
			closeGame();
			animateNum();
		}

		function closeGame() {
			btnCloseGame.on('click', function(e) {
				e.preventDefault();
				popupGame.removeClass('game-ready');
				visibleScroll();
				destroyAnimate();
				setTimeout(() => {
					point.text(0);
				}, 600);
			});
		}

		function animateNum() {
			let animPoint = setInterval(() => {
				if (count <= point.data('point')) {
					point.text(count++);
				} else {
					clearInterval(animPoint);
					count = 0;
				}
			}, 100);
		}

		function initAnimate() {
			const blockAnimate = wrap[0].querySelector('.profile__bottom');

			wrap.addClass('section--animate');
			blockAnimate.addEventListener('animationend', openGame, false);

			if (cookieFn.getCookie('AlpenGold') === undefined) cookies.addClass('js-popup-show');
		}

		function destroyAnimate() {
			wrap.removeClass('section--animate');
		}

		return {
			scrollTo: scrollTo,
			playGame: playGame
		};
		
	}) ();

	if (Boolean(document.getElementById('profile')) !== Boolean(null)) {
		clickAnimate.scrollTo();
		clickAnimate.playGame();
	}

	// Init tooltips
	(function initTooltips() {
		const tooltipsMob = $('#popup-game').find('.popup__game-tooltip-btn');
		const tooltipsDesk = $('#popup-game').find('.popup__level-info');

		function initTooltipster(el, trigger) {
			el.tooltipster({
				animation: 'fade',
				delay: 200,
				distance: 1,
				side: ['right', 'left', 'bottom', 'top'],
				maxWidth: 215,
				minWidth: 215,
				zIndex: 11,
				IEmin: 11,
				trigger: trigger,
				triggerOpen: {
					click: true,
					tap: true
				},
				triggerClose: {
					click: true,
					tap: true
				}
			});
		}

		if (Boolean('ontouchstart' in window) || Boolean(navigator.msMaxTouchPoints)) {
			tooltipsMob.parent().addClass('popup__game-tooltip--active');
			initTooltipster(tooltipsMob, 'custom');
			$.tooltipster.on('created', function(e) {
				$(e.origin).addClass('active');
			});
			$.tooltipster.on('close', function(e) {
				$(e.origin).removeClass('active');
			});
		} else {
			initTooltipster(tooltipsDesk, 'hover');
		}
	}) ();

	// Init img-scaling
	const initImgScaling = (() => {
		const img = document.getElementById('img-scaling');

		function imgScalingFn() {
			if ('IntersectionObserver' in window) {
				const imageScaling = new IntersectionObserver(function(entries, observer) {
					let entry = entries[0];
					
					if (entry.isIntersecting) {
						scrollingForHeight();
						$(window).on('scroll.scaling', scrollingForHeight);
					} else $(window).off('scroll.scaling');
				});
	
				imageScaling.observe(img);
			} else return;
		}

		function scrollingForHeight() {
			let windowTop = $(window).scrollTop();
			let offset = $(img).offset();
			let scrollPercent = (offset.top + windowTop) / offset.top - .5;
			let scaling = scrollPercent.toFixed(3);

			if (Boolean(scaling >= 1) && Boolean(scaling <= 1.4)) $(img).css('transform', `translateX(-50%) scale(${scaling})`);
		}

		return {
			imgScalingFn: imgScalingFn
		};
	}) ();

	if (Boolean(document.getElementById('img-scaling')) !== Boolean(null)) initImgScaling.imgScalingFn();

	// Validate
	(function initValidate() {
		const form = $('.form');

		$.each(form, function() {
			$(this).validate({
				ignore: [],
				errorClass: 'error',
				validClass: 'success',
				rules: {
					phone: {
						required: true,
						phone: true 
					},
					password: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						} 
					},
					email: {
						required: true,
						email: true 
					},
					name: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					surname: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					theme: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					code: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					captcha: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					card: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					shops: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
				},
				messages: {
					phone: 'Некорректный номер',
					rules: {
						required: '' 
					},
					personalAgreement: {
						required: '' 
					}
				},
				highlight: function(el) {
					let parent = $(el).closest('.input-wrap');
					parent.addClass('error');
				},
				unhighlight: function(el) {
					let parent = $(el).closest('.input-wrap');
					parent.removeClass('error');
				}
			});

			jQuery.validator.addMethod('phone', function (value, element) {
				return this.optional(element) || /\+7\s\(\d{3}\)\s\d{3}-\d{4}/.test(value);
			});
		
			jQuery.validator.addMethod('email', function (value, element) {
				return this.optional(element) || /\w+@[a-zA-Z_-]+?\.[a-zA-Z]{2,6}/.test(value);
			});
		});
	}) ();
});
