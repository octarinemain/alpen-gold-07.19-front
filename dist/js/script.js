/* global Swiper moment OverlayScrollbars Rellax*/
'use strict';
$(document).ready(function () {

  function hiddenScroll() {
    if ($(document).height() > $(window).height()) {
      var scrollTop = $('html').scrollTop() ? $('html').scrollTop() : $('body').scrollTop();
      $('html').addClass('no-scroll').css('top', -scrollTop);
    }
  }

  function visibleScroll() {
    var scrollTop = parseInt($('html').css('top'));
    $('html').removeClass('no-scroll');
    $('html, body').scrollTop(-scrollTop);
  }

  // Init popups
  (function initPopups() {
    var btnOpenPopups = $('.js-popup-button');
    var btnClosePopups = $('.js-close-popup');
    var popupGame = $('#popup-game');
    var gameMine = $('#game-mine');

    function initPopup() {
      btnOpenPopups.on('click', function (e) {
        e.preventDefault();
        if (!gameMine.hasClass('js-popup-show')) $('.popup').removeClass('js-popup-show');
        var popupClass = ".".concat($(this).attr('data-popup'));
        $(popupClass).addClass('js-popup-show');
        if (!$('html').hasClass('no-scroll')) hiddenScroll();
      });
      closePopup();
    }

    function closePopup() {
      btnClosePopups.on('click', function (e) {
        e.preventDefault();
        $('.popup').removeClass('js-popup-show');
        if ($('html').hasClass('no-scroll') && popupGame.hasClass('game-ready')) return;
        if ($('html').hasClass('no-scroll')) visibleScroll();
      });
    }
    initPopup();
  })();

  // Init dropdown
  (function initDropdown() {
    var dropdown = $('.dropdown');

    dropdown.on('click', '.js-btn-dropdown', function (e) {
      e.preventDefault();
      var list = $(this).
      closest('.dropdown').
      find('.dropdown__inner').
      toggleClass('js-dropdown-open');

      if (list[0].style.maxHeight) list[0].style.maxHeight = null;else
      list[0].style.maxHeight = "".concat(list[0].scrollHeight, "px");

      if (list.hasClass('js-dropdown-open')) {
        var width = $(window).width();

        $(window).on('resize.dropdown', function () {
          var widthResize = $(window).width();

          if (width !== widthResize) {
            list.removeClass('js-dropdown-open');
            list[0].style.maxHeight = null;
          } else {
            return $(window).off('resize.dropdown');
          }
        });
      }
    });
  })();

  // Init more shops
  (function initMoreShops() {
    var wrap = $('#shop-list');
    var parentWrap = wrap.parent();
    var link = wrap.find('.shop-link');
    var dropdown = wrap.find('.dropdown');
    var dropdownBtnImg = dropdown.find('.shop-list__btn-dropdown img');
    var dropdownList = dropdown.find('.dropdown__list');
    link.clone().
    removeClass('shop-item').
    appendTo(dropdownList).
    wrap('<li class="shop-item"></li>');
    var dropdownItems = dropdownList.find('li');
    var allItems = wrap.find('.shop-item');

    var doAdapt = function doAdapt() {
      allItems.each(function () {
        $(this).removeClass('hidden');
      });

      var btnWidth = dropdown.outerWidth(true);
      var hiddenItems = [];
      var wrapWidth = parseInt(wrap.width());

      link.each(function (i, item) {
        if (wrapWidth >= btnWidth + $(item).outerWidth(true) + 5) {
          btnWidth += $(item).outerWidth(true) + 5;
        } else {
          $(item).addClass('hidden');
          hiddenItems.push(i);
        }
      });

      var addedSrc = false;

      if (!hiddenItems.length && !dropdown.hasClass('hidden')) {
        dropdown.addClass('hidden');
      } else {
        dropdownItems.each(function (i, item) {
          if (!~hiddenItems.indexOf(i)) $(item).addClass('hidden');

          if (!$(item).hasClass('hidden') && !addedSrc) {
            addedSrc = true;
            var curSrc = $(item).find('img').attr('src');
            dropdownBtnImg.attr('src', curSrc);
          }
        });
      }
    };

    setTimeout(function () {
      doAdapt();
      parentWrap.removeClass('header__top-inner--hidden');
    }, 50);

    $(window).on('resize.shops', function () {
      doAdapt();
    });

  })();

  // Open burger-menu
  (function openBurger() {
    var wrap = $('#header-bottom');
    var burger = wrap.find('.burger-menu');

    burger.on('click', function (e) {
      e.preventDefault();
      $(this).toggleClass('active').
      siblings('.nav').
      toggleClass('active');
    });
  })();

  // Init timer
  (function () {
    // Finish Date YYYY-MM-DD
    var countDownDate = moment('2019-08-17');

    var countdown_d = $('#countdown-d');
    var countdown_h = $('#countdown-h');
    var countdown_m = $('#countdown-m');
    var countdown_s = $('#countdown-s');

    // Method for normal view date
    var viewCount = function viewCount(count) {return count < 10 ? "0".concat(count) : count;};

    var countDown = setInterval(function () {
      if (!$('.countdown').hasClass('active')) $('.countdown').addClass('active');

      var now = moment(new Date());
      var distance = countDownDate.diff(now);

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
      var minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
      var seconds = Math.floor(distance % (1000 * 60) / 1000);

      // Display the result in the elements
      countdown_d.text(days);
      countdown_h.text(viewCount(hours));
      countdown_m.text(viewCount(minutes));
      countdown_s.text(viewCount(seconds));

      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(countDown);

        countdown_d.text('--');
        countdown_h.text('--');
        countdown_m.text('--');
        countdown_s.text('--');
      }
    }, 1000);
  })();

  // Anim for Head section
  (function () {if ($('.head__gold-img').length) $('.head__gold-img').addClass('active');})();

  // Anim for Head chocolate
  (function () {if ($('.head__chocolate--animate').length) $('.head__chocolate--animate').addClass('active');})();

  // Prize section slider
  (function () {
    new Swiper('.prize-main__content', {
      slidesPerView: 3,
      spaceBetween: 0,
      simulateTouch: false,
      navigation: {
        nextEl: '.prize-main__inner .swiper-custom-button-next',
        prevEl: '.prize-main__inner .swiper-custom-button-prev' },

      breakpoints: {
        767: {
          slidesPerView: 2 },

        479: {
          slidesPerView: 1 } } });



  })();

  // Promo section slider
  (function () {
    new Swiper('.promo-info__content', {
      slidesPerView: 1,
      resistanceRatio: 0,
      navigation: {
        nextEl: '.promo-info__inner .swiper-custom-button-next',
        prevEl: '.promo-info__inner .swiper-custom-button-prev' } });


  })();

  // Init parallax
  (function () {
    $(window).on('scroll', function () {
      var scrolled = $(window).scrollTop();
      $('.coin-img--first, .coin-img--fourth').css('transform', "translateY(".concat(scrolled * .05, "px)"));
      $('.coin-img--second').css('transform', "translateY(".concat(scrolled * .1, "px)"));
      $('.coin-img--third').css('transform', "translateY(".concat(scrolled * .2, "px)"));
    });
  })();

  (function () {
    var wrap = $('.products-slider');

    new Swiper(wrap, {
      slidesPerView: 4,
      spaceBetween: 60,
      watchOverflow: true,
      centerInsufficientSlides: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev' },

      breakpoints: {
        991: {
          slidesPerView: 3 },

        767: {
          slidesPerView: 2 },

        479: {
          slidesPerView: 1 } } });



  })();

  // Init scroll
  var initScroll = function () {
    var wrapTable = document.getElementById('winner-table');
    var wrapPopupTabel = document.getElementById('popup-winner-table');
    var faqInner = document.getElementById('faq-inner');

    var initCustomScrollWinners = function initCustomScrollWinners() {return initCustomScroll(wrapTable);};
    var initCustomScrollPopupWinners = function initCustomScrollPopupWinners() {return initCustomScroll(wrapPopupTabel);};
    var initCustomScrollFaq = function initCustomScrollFaq() {return initCustomScroll(faqInner);};

    function initCustomScroll(el) {
      OverlayScrollbars(el, {
        scrollbars: {
          touchSupport: false } });


    }

    return {
      initCustomScrollWinners: initCustomScrollWinners,
      initCustomScrollPopupWinners: initCustomScrollPopupWinners,
      initCustomScrollFaq: initCustomScrollFaq };

  }();

  if (Boolean(document.getElementById('winner-table')) !== Boolean(null)) initScroll.initCustomScrollWinners();
  if (Boolean(document.getElementById('popup-winner-table')) !== Boolean(null)) initScroll.initCustomScrollPopupWinners();
  if (Boolean(document.getElementById('faq-inner')) !== Boolean(null)) initScroll.initCustomScrollFaq();

  // init accordion
  (function initAccordion() {
    var list = $('#faq-inner').find('.faq__list');

    list.on('click', '.faq__title', function () {
      var $this = $(this);
      var parent = $this.parent();
      var content = $this.next();

      if (content.is(':visible')) {
        content.slideUp('fast');
        parent.removeClass('active');
      } else {
        content.slideDown('fast');
        parent.addClass('active');
      }

    });
  })();

  //Init mask
  (function initMask() {
    var inputCard = $('#search-card').find('input');
    var inputTel = $('input[type="tel"]');

    inputTel.mask('+7 (999) 999-9999', {
      autoclear: false });


    inputCard.mask('9999 9999 9999 9999', {
      autoclear: false,
      placeholder: 'X' });

  })();

  // Init video
  var initVideo = function () {
    var wrap = document.getElementById('winners-slider');

    // function playVideo() {
    // 	const videos = wrap.querySelectorAll('.winners__video');

    // 	$(videos).fancybox({
    // 		keyboard: false,
    // 		arrows: false,
    // 		infobar: false,
    // 		buttons: ['close'],
    // 		touch: false,
    // 		wheel: false,
    // 		youtube : {
    // 			showinfo: 0
    // 		},
    // 	});

    // 	for (let i = 0; i < videos.length; i++) {
    // 		let src = `https://img.youtube.com/vi/${videos[i].dataset.embed}/sddefault.jpg`;
    // 		let img = new Image();
    // 		img.addEventListener('load', function() {
    // 			videos[i].appendChild(img);
    // 		}, false);
    // 		img.src = src;
    // 	}
    // }

    function fnSlider() {
      new Swiper(wrap, {
        slidesPerView: 3,
        spaceBetween: 40,
        allowTouchMove: false,
        navigation: {
          nextEl: '.swiper-custom-button-next',
          prevEl: '.swiper-custom-button-prev' },

        breakpoints: {
          767: {
            slidesPerView: 1,
            spaceBetween: 15,
            allowTouchMove: true } } });



    }

    return {
      // playVideo: playVideo,
      fnSlider: fnSlider };

  }();

  if (document.getElementById('winners-slider') !== null) {
    // initVideo.playVideo();
    initVideo.fnSlider();
  }

  // Init calendar
  var initCalendar = function () {
    var wrap = $('#calendar-wrap');

    function calendarFn() {
      var calendar = wrap.find('.calendar__input');

      var datepicker = calendar.datepicker({
        showOtherMonths: false,
        dateFormat: 'dd.mm.yyyy',
        position: '',
        offest: 0,
        autoClose: true }).
      data('datepicker');

      var curDate = datepicker.date;
      var day = curDate.getDate();
      var month = curDate.getMonth() + 1;
      var year = curDate.getFullYear();
      if (day < 10) day = "0".concat(day);
      if (month < 10) month = "0".concat(month);
      var startDate = "".concat(day, ".").concat(month, ".").concat(year);
      calendar.val(startDate);
      var wrapDatepicker = $('#datepickers-container');
      wrapDatepicker.appendTo(wrap);
    }

    return {
      calendarFn: calendarFn };


  }();

  if (document.getElementById('calendar-wrap') !== null) initCalendar.calendarFn();

  (function () {
    $('.head__countdown-close').on('click', function () {
      $('.head__countdown').remove();
    });
  })();

  // init notifications
  (function initNotif() {
    var notif = $('.notification__item');

    notif.find('.notification__close').on('click', function (e) {
      e.preventDefault();
      if ($(this).parent().hasClass('active')) $(this).parent().removeClass('active');
    });
  })();

  // add active btn
  (function addActiveBtn() {
    var wrap = $('#search');
    var inputTel = wrap.find('input[type="tel"]');
    var inputCard = wrap.find('input[type="text"]');
    var btn = wrap.find('button');
    var regPhone = /\+7\s\(\d{3}\)\s\d{3}-\d{4}/;
    var regCard = /\d{4}\s\d{4}\s\d{4}\s\d{4}/;

    inputTel.on('keyup', inputFn.bind(inputTel, btn, regPhone));
    inputCard.on('keyup', inputFn.bind(inputCard, btn, regCard));

    function inputFn(el, reg) {
      var $this = $(this);
      var value = $this.val();
      var valid = reg.test(value);

      if (valid && !el.hasClass('active')) el.addClass('active');else
      if (!valid && el.hasClass('active')) el.removeClass('active');
    }
  })();

  // init select2
  (function initSelect2() {
    var select = $('select');

    select.each(function () {
      var $this = $(this);
      var parent = $this.parent();
      var selectTheme = parent.find('select[name="theme"]');
      var selectShop = parent.find('select[name="shops"]');

      customSelectFn(selectTheme, 'Выберете тему');
      customSelectFn(selectShop, 'Выбрать');

      function customSelectFn(select, str) {
        select.select2({
          placeholder: str,
          minimumResultsForSearch: -1,
          dropdownParent: parent });

      }
    });


  })();

  // open checks
  (function openChecks() {
    var checks = $('.box-checks__img');

    checks.fancybox({
      keyboard: false,
      arrows: false,
      infobar: false,
      buttons: ['close'],
      touch: false,
      wheel: false });

  })();

  // init tabs
  (function initTabs() {
    var wrap = $('#profile-center');
    var tabs = wrap.find('.tabs');
    var tabsItem = wrap.find('.tabs__item--tab');
    var tabsContent = wrap.find('.tabs-content__item');

    tabs.on('click', '.tabs__item--tab', function (e) {
      e.preventDefault();
      var $this = $(this);
      var i = $this.index('.tabs__item--tab');

      tabsItem.removeClass('active');
      $this.addClass('active');
      tabsContent.removeClass('active').eq(i).addClass('active');
    });
  })();

  // Cookies
  var cookieFn = function () {
    var cookies = $('.cookies');
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    cookies.find('.btn--cookies').on('click', function (e) {
      e.preventDefault();
      cookies.removeClass('js-popup-show');
      document.cookie = "AlpenGold=Game; path=/; expires=".concat(date.toUTCString());
    });

    function getCookie(name) {
      var matches = document.cookie.match(new RegExp("(?:^|; )".concat(
      name.replace(/([.$?*|{}()[]\\\/\+^])/g, '\\$1'), "=([^;]*)")));

      return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    return {
      getCookie: getCookie };

  }();

  // click animate
  var clickAnimate = function () {
    var wrap = $('#profile');
    var btnScroll = wrap.find('.tabs__item--play a');
    var btnStartGame = wrap.find('.btn--play');
    var popupGame = $('#popup-game');
    var btnCloseGame = popupGame.find('.popup__close--game');
    var point = $('#popup-points').find('.popup__point-num[data-point]');
    var cookies = $('.cookies');
    var count = 0;

    function scrollTo() {
      btnScroll.on('click', function () {
        var wrapHeight = wrap.outerHeight();
        var wrapPos = wrap.offset().top;
        var viewportHeight = $(window).height();

        $('html, body').stop().animate({
          scrollTop: wrapHeight + wrapPos - viewportHeight },
        700, function () {
          initAnimate();
        });
        return false;
      });
    }

    function playGame() {
      btnStartGame.on('click', function (e) {
        e.preventDefault();
        initAnimate();
      });
    }

    function openGame() {
      popupGame.addClass('game-ready');
      hiddenScroll();
      closeGame();
      animateNum();
    }

    function closeGame() {
      btnCloseGame.on('click', function (e) {
        e.preventDefault();
        popupGame.removeClass('game-ready');
        visibleScroll();
        destroyAnimate();
        setTimeout(function () {
          point.text(0);
        }, 600);
      });
    }

    function animateNum() {
      var animPoint = setInterval(function () {
        if (count <= point.data('point')) {
          point.text(count++);
        } else {
          clearInterval(animPoint);
          count = 0;
        }
      }, 100);
    }

    function initAnimate() {
      var blockAnimate = wrap[0].querySelector('.profile__bottom');

      wrap.addClass('section--animate');
      blockAnimate.addEventListener('animationend', openGame, false);

      if (cookieFn.getCookie('AlpenGold') === undefined) cookies.addClass('js-popup-show');
    }

    function destroyAnimate() {
      wrap.removeClass('section--animate');
    }

    return {
      scrollTo: scrollTo,
      playGame: playGame };


  }();

  if (Boolean(document.getElementById('profile')) !== Boolean(null)) {
    clickAnimate.scrollTo();
    clickAnimate.playGame();
  }

  // Init tooltips
  (function initTooltips() {
    var tooltipsMob = $('#popup-game').find('.popup__game-tooltip-btn');
    var tooltipsDesk = $('#popup-game').find('.popup__level-info');

    function initTooltipster(el, trigger) {
      el.tooltipster({
        animation: 'fade',
        delay: 200,
        distance: 1,
        side: ['right', 'left', 'bottom', 'top'],
        maxWidth: 215,
        minWidth: 215,
        zIndex: 11,
        IEmin: 11,
        trigger: trigger,
        triggerOpen: {
          click: true,
          tap: true },

        triggerClose: {
          click: true,
          tap: true } });


    }

    if (Boolean('ontouchstart' in window) || Boolean(navigator.msMaxTouchPoints)) {
      tooltipsMob.parent().addClass('popup__game-tooltip--active');
      initTooltipster(tooltipsMob, 'custom');
      $.tooltipster.on('created', function (e) {
        $(e.origin).addClass('active');
      });
      $.tooltipster.on('close', function (e) {
        $(e.origin).removeClass('active');
      });
    } else {
      initTooltipster(tooltipsDesk, 'hover');
    }
  })();

  // Init img-scaling
  var initImgScaling = function () {
    var img = document.getElementById('img-scaling');

    function imgScalingFn() {
      if ('IntersectionObserver' in window) {
        var imageScaling = new IntersectionObserver(function (entries, observer) {
          var entry = entries[0];

          if (entry.isIntersecting) {
            scrollingForHeight();
            $(window).on('scroll.scaling', scrollingForHeight);
          } else $(window).off('scroll.scaling');
        });

        imageScaling.observe(img);
      } else return;
    }

    function scrollingForHeight() {
      var windowTop = $(window).scrollTop();
      var offset = $(img).offset();
      var scrollPercent = (offset.top + windowTop) / offset.top - .5;
      var scaling = scrollPercent.toFixed(3);

      if (Boolean(scaling >= 1) && Boolean(scaling <= 1.4)) $(img).css('transform', "translateX(-50%) scale(".concat(scaling, ")"));
    }

    return {
      imgScalingFn: imgScalingFn };

  }();

  if (Boolean(document.getElementById('img-scaling')) !== Boolean(null)) initImgScaling.imgScalingFn();

  // Validate
  (function initValidate() {
    var form = $('.form');

    $.each(form, function () {
      $(this).validate({
        ignore: [],
        errorClass: 'error',
        validClass: 'success',
        rules: {
          phone: {
            required: true,
            phone: true },

          password: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          email: {
            required: true,
            email: true },

          name: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          surname: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          theme: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          code: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          captcha: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          card: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          shops: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } } },


        messages: {
          phone: 'Некорректный номер',
          rules: {
            required: '' },

          personalAgreement: {
            required: '' } },


        highlight: function highlight(el) {
          var parent = $(el).closest('.input-wrap');
          parent.addClass('error');
        },
        unhighlight: function unhighlight(el) {
          var parent = $(el).closest('.input-wrap');
          parent.removeClass('error');
        } });


      jQuery.validator.addMethod('phone', function (value, element) {
        return this.optional(element) || /\+7\s\(\d{3}\)\s\d{3}-\d{4}/.test(value);
      });

      jQuery.validator.addMethod('email', function (value, element) {
        return this.optional(element) || /\w+@[a-zA-Z_-]+?\.[a-zA-Z]{2,6}/.test(value);
      });
    });
  })();
});